/*
 */
package pl.outerspace.chatclient;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;
import pl.outerspace.chatclient.gui.MainFrame;
import pl.outerspace.chatclient.model.ChatMessage;

/**
 *
 * @author tku
 */
public class Chat {

    private WebsocketClientEndpoint clientEndpoint;
    private JTextArea messages;

    public void init() {
        try {
            clientEndpoint = new WebsocketClientEndpoint(new URI("ws://localhost:8080/ChatServer/chat"));
            // Add listener.
            clientEndpoint.addMessageHandler(new WebsocketClientEndpoint.MessageHandler() {
                @Override
                public void handleMessage(String message) {
                    ChatMessage msg = new ChatMessage();
                    msg.setJson(message);
                    messages.append(String.format("%s\n%s: %s\n", msg.getTimestamp(), msg.getUser(), msg.getMessage()));
                    //System.out.println(message);
                }
            });
        } catch (URISyntaxException ex) {
            Logger.getLogger(Chat.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setJTextArea(JTextArea messages) {
        this.messages = messages;
    }

    public void sendMessage(ChatMessage msg) {
        clientEndpoint.sendMessage(msg.getJson());
    }

    public static void main(String[] args) {
        Chat chat = new Chat();
        chat.init();

        MainFrame mainFrame = new MainFrame();
        mainFrame.setChat(chat);
        mainFrame.setVisible(true);
    }
}
