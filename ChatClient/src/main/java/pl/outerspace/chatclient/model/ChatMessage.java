/*
 */
package pl.outerspace.chatclient.model;

import java.io.Serializable;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author tku
 */
public class ChatMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    private String user;

    private String message;

    private String timestamp = null;

    public ChatMessage() {
        super();
    }

    public ChatMessage(String user, String message) {
        this.user = user;
        this.message = message;
    }

    public ChatMessage(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    
    public String getJson() {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("user", user)
                .add("message", message)
                .add("timestamp", new SimpleDateFormat("dd/MM/yyyy h:mm:ss a z").format(new Date()))
                .build();

        return jsonObject.toString();
    }
    
    public void setJson(String json) {
        try (JsonReader jsonReader = Json.createReader(new StringReader(json))) {
            JsonObject jsonObject = jsonReader.readObject();
            user = jsonObject.getString("user");
            message = jsonObject.getString("message");
            timestamp = jsonObject.getString("timestamp");
        }
    }

    @Override
    public String toString() {
        return "ChatMessage{" + "user=" + user + ", message=" + message + '}';
    }
}
