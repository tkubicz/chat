/*
 */
package pl.outerspace.chatserver;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 *
 * @author tku
 */
public class ChatMessage implements Decoder.Text<ChatMessage>, Encoder.Text<ChatMessage> {

    @NotNull
    @Size(min = 1, max = 42, message = "User must be between 1 and 42 characters")
    private String user;

    @NotNull
    @Size(min = 1, max = 255, message = "Message must be between 1 and 255 characters")
    private String message;

    @Override
    public void init(EndpointConfig config) {
        // Nothng to do.
    }

    @Override
    public ChatMessage decode(String value) throws DecodeException {
        try (JsonReader jsonReader = Json.createReader(new StringReader(value))) {
            JsonObject jsonObject = jsonReader.readObject();
            user = jsonObject.getString("user");
            message = jsonObject.getString("message");
        }

        return this;
    }

    @Override
    public String encode(ChatMessage chatMessage) throws EncodeException {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("user", chatMessage.user)
                .add("message", chatMessage.message)
                .add("timestamp", new SimpleDateFormat("dd/MM/yyyy h:mm:ss a z").format(new Date()))
                .build();

        return jsonObject.toString();
    }

    @Override
    public boolean willDecode(String arg0) {
        // Detect if it's a valid format.
        return true;
    }

    @Override
    public void destroy() {
        // Nothing to do.
    }

    @Override
    public String toString() {
        return "ChatMessage{" + "user=" + user + ", message=" + message + '}';
    }
}
