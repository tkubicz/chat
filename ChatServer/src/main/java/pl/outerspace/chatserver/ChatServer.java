/*
 */
package pl.outerspace.chatserver;

import java.util.logging.Logger;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import javax.ejb.Singleton;
import javax.json.Json;
import javax.json.JsonObject;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author tku
 */
@ServerEndpoint(value = "/chat", encoders = {ChatMessage.class}, decoders = {ChatMessage.class})
@Singleton
public class ChatServer {

    /**
     * Logger.
     */
    private static final Logger logger = Logger.getLogger(ChatServer.class.getName());
    
    private final Set<Session> peers;
    
    public ChatServer() {
        peers = new HashSet<>();
    }
    
    @OnOpen
    public void onOpen(Session peer) {
        logger.log(Level.INFO, "Opened session: {0}", peer);
        peers.add(peer);
    }
    
    @OnClose
    public void onClose(Session peer) {
        logger.log(Level.INFO, "Closed session {0}", peer);
        peers.remove(peer);
    }
    
    @OnMessage
    public void onMessage(@Valid ChatMessage message, Session session) {
        logger.log(Level.INFO, "Recived message {0} from peer {1}", new Object[]{message, session});
        
        for (Session peer : peers) {
            try {
                logger.log(Level.INFO, "Broadcastng message {0} to peer {1}", new Object[]{message, peer});
                peer.getBasicRemote().sendObject(message);
            } catch (IOException | EncodeException e) {
                logger.log(Level.SEVERE, "Error sending message", e);
            }
        }
    }
    
    @OnError
    public void onError(Session session, Throwable error) {
        try {
            if (error.getCause() instanceof ConstraintViolationException) {
                // Just report the first validation problem.
                JsonObject jsonObject = Json.createObjectBuilder()
                        .add("error",
                                ((ConstraintViolationException) error.getCause())
                                .getConstraintViolations().iterator().next()
                                .getMessage())
                        .build();
                session.getBasicRemote().sendText(jsonObject.toString());
            } else {
                logger.log(Level.SEVERE, null, error);
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, null, e);
        }
    }
}
