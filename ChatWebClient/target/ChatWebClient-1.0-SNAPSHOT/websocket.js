//var wsUri = "ws://" + document.location.host + document.location.pathname + "chat";
var wsUri = "ws://" + document.location.host + "/ChatServer/chat";
var websocket = new WebSocket(wsUri);

websocket.onerror = function(evt) { onError(evt) };

function onError(evt) {
    writeToScreen('<span style="color: red;">Error</span> ' + evt.data);
}

// For testing purposes
var output = document.getElementById("output");
websocket.onopen = function(evt) { onOpen(evt) };

function writeToScreen(message) {
    output.innerHTML += message + "<br>";
}

function onOpen() {
    writeToScreen("Connected to " + wsUri);
}
// End test functions

function sendMessage(json) {
    console.log("sending text: " + json);
    websocket.send(json);
}

websocket.onmessage = function(evt) { onMessage(evt) };

function onMessage(evt) {
    console.log("recived: " + evt.data);
    displayChat(evt.data);
}