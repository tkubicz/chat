function sendAlert() {
    var message = document.getElementById("message").value;
    var username = document.getElementById("username").value;
    
    var json = JSON.stringify({
      'user': username,
      'message': message,
      'timestamp': '13/03/2015 3:53:42 PM CET'
    });
    
    sendMessage(json);
}

function displayChat(data) {
    var json = JSON.parse(data);
    var chat = document.getElementById("chatWindow");
    chat.innerHTML = chat.innerHTML + json.timestamp + '\n' + json.user + ': ' + json.message + '\n';
}