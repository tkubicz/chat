package pl.outerspace.chatandroidclient.adapters;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import pl.outerspace.chatandroidclient.R;
import pl.outerspace.chatandroidclient.model.ChatMessage;

/**
 * Created by tku on 15.03.15.
 */
public class Chat2MessageAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ChatMessage> messages;

    public Chat2MessageAdapter(Context context, ArrayList<ChatMessage> messages) {
        super();
        this.context = context;
        this.messages = messages;
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int i) {
        return messages.get(i);
    }

    @Override
    public long getItemId(int i) {
        // Unimplemented, we aren't using Sqlite.
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ChatMessage message = (ChatMessage) this.getItem(position);

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.activity_chat2_msg_row, parent, false);
            holder.message = (TextView) convertView.findViewById(R.id.message_text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.message.setText(message.getMessage());

        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) holder.message.getLayoutParams();

        // Check whether message is mine to show green background and align right
        if (message.getTimestamp() == null) {
            holder.message.setText("You: " + message.getMessage());
            holder.message.setTextColor(0xFF9FB3D1);
            lp.gravity = Gravity.RIGHT;
        }
        // If not mine, then it is from sender and align to left
        else {
            holder.message.setText(message.getUser() + ": " + message.getMessage());
            holder.message.setTextColor(0xFFA1E77);
            lp.gravity = Gravity.LEFT;
        }
        holder.message.setLayoutParams(lp);

        return convertView;
    }

    private static class ViewHolder {
        TextView message;
    }
}
