package pl.outerspace.chatandroidclient.model;

/**
 * Created by tku on 15.03.15.
 */
public class ChatMessage {
    private static final long serialVersionUID = 1L;

    private String user;
    private String message;
    private String timestamp = null;

    public ChatMessage() {
        super();
    }

    public ChatMessage(String user) {
        this.user = user;
    }

    public ChatMessage(String user, String message) {
        this.user = user;
        this.message = message;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "ChatMessage{" +
                "user='" + user + '\'' +
                ", message='" + message + '\'' +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}


