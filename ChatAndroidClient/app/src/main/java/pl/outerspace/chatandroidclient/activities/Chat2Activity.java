package pl.outerspace.chatandroidclient.activities;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

import org.codehaus.jackson.map.ObjectMapper;

import java.util.ArrayList;

import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;
import pl.outerspace.chatandroidclient.AppContext;
import pl.outerspace.chatandroidclient.R;
import pl.outerspace.chatandroidclient.adapters.Chat2MessageAdapter;
import pl.outerspace.chatandroidclient.model.ChatMessage;

public class Chat2Activity extends ListActivity {

    static final String TAG = Chat2Activity.class.getName();

    private ArrayList<ChatMessage> messages = new ArrayList<ChatMessage>();
    private Chat2MessageAdapter adapter;
    private EditText text;
    private final ObjectMapper mapper = new ObjectMapper();
    private final WebSocketConnection connection = new WebSocketConnection();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat2);

        AppContext.getInstance().setUsername("android");

        text = (EditText) this.findViewById(R.id.text);
        adapter = new Chat2MessageAdapter(this, messages);
        setListAdapter(adapter);
        start();
    }

    private void start() {
        final String wsuri = getString(R.string.chat_url);

        Log.d(TAG, "Status: Connecting to " + wsuri + " ..");

        try {
            connection.connect(wsuri, new WebSocketHandler() {
                @Override
                public void onOpen() {
                    Log.d(TAG, "Connected to " + wsuri + " ..");

                    text.setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View view, int keyCode, KeyEvent event) {
                            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                                switch (keyCode) {
                                    case KeyEvent.KEYCODE_DPAD_CENTER:
                                    case KeyEvent.KEYCODE_ENTER:
                                        ChatMessage msg = new ChatMessage(AppContext.getInstance().getUsername(), text.getText().toString());
                                        Chat2Activity.this.addNewMessage(msg);
                                        try {
                                            String jsonMessage = mapper.writeValueAsString(msg);
                                            Log.d(TAG, "Sending message: " + jsonMessage);
                                            connection.sendTextMessage(jsonMessage);
                                            text.setText("");
                                        } catch (Exception ex) {
                                            Log.d(TAG, "Parsing to Json string failed: " + ex.getMessage());
                                        }
                                        return true;
                                    default:
                                        break;
                                }
                            }
                            return false;
                        }
                    });
                }

                @Override
                public void onTextMessage(String payload) {
                    Log.d(TAG, "Got back: " + payload);
                    try {
                        addNewMessage(mapper.readValue(payload, ChatMessage.class));
                    } catch (Exception ex) {
                        Log.d(TAG, "Json Parsing Exception: " + ex);
                    }
                }

                @Override
                public void onClose(int code, String reason) {
                    Log.d(TAG, "Connection closed or lost");
                }
            });
        } catch (WebSocketException e) {
            Log.d(TAG, e.toString());
        }
    }

    void addNewMessage(ChatMessage msg) {
        messages.add(msg);
        adapter.notifyDataSetChanged();
        getListView().setSelection(messages.size() - 1);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat2, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (connection.isConnected()) {
            connection.disconnect();
        }
    }
}
